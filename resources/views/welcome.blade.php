@extends('layouts.app')

@section('content')
<img src="https://codebrisk.com/assets/images/posts/1625808060_laravel-banner1.webp" class="img-fluid mx-5 px-2">
<div class="container my-5">
    <div class="row justify-content-center">

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Featured Posts') }}</div>
                    @foreach($posts as $post)
                        <div class="card-body">
                            <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                            <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                            <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                            {{-- check if there is an authenticated user to prevent our web app from throwing an error when no user is logged in --}}
                        </div>
                    @endforeach
            </div>
        </div>
    </div>
</div>
@endsection