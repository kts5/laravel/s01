@extends('layouts.app')

@section('content')

	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{ $post->title }}</h2>

			<p class="card-subtitle">Author: {{ $post->user->name }}</p>

			<p class="card-subtitle text-muted mb-3">Created at: {{ $post->created_at }}</p>

			<p class="card-text"> {{ $post->content }}</p>

			@if(Auth::id())
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					    @method('PUT')
					    @csrf
					    @if($post->likes->contains("user_id", Auth::id()))
					        <button type="submit" class="btn btn-danger">Unlike</button>
					    @else
					        <button type="submit" class="btn btn-success">Like</button>
					    @endif
					</form>

					<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
					  Add Comment
					</button>
				@endif
			@endif



			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
		<div class="card-footer">
			@if(count($post->comments) > 0)
			    @foreach($post->comments as $comment)
			    	<div class="container my-3">
			    		<p class="card-text m-0">{{$comment->user->name}}</p>
						<p class="card-text">{{$comment->content}}</p>
			    	</div>			    	
				@endforeach
			@endif
		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
	        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<form method="POST" action="/posts/{{$post->id}}/comment">
	      		@csrf
	      		<div class="form-group">
	      			<label for="content">Content:</label>
	      			<textarea class="form-control" name="content" id="content" rows="3"></textarea>
	      		</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
	      	</form>   
	    </div>
	  </div>
	</div>

@endsection