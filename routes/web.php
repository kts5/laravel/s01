<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', [PostController::class, 'welcome']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein we can view create post and it will be returned to the user
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein form data will be sent via POST method to the /posts URI endpoint
Route::post('/posts', [PostController::class, 'store']);

// define route that will return a vew containing all posts
Route::get('/posts', [PostController::class, 'index']);


// define a route taht will return view containing only the authenticated user's posts
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

// define a route for viewing the edit
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// define a route that will overwrite an existing post with matching URL paramater
Route::put('/posts/{id}', [PostController::class, 'update']);

// define a route taht will delete a post
Route::delete('/posts/{id}', [PostController::class, 'archive']);


Route::put('/posts/{id}/like', [PostController::class, 'like']);


Route::post('/posts/{id}/comment', [PostController::class, 'comment']);